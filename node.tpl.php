  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><h2><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    <span class="postdata"><?php print $submitted?></span>
    <div class="content"><?php print $content?></div>
    <?php if ($links) { ?><div class="postmetadata"><span class="taxonomy"><?php print $terms?></span><?php print $links?></div><?php }; ?>
  </div>
