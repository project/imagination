<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body>
<div id="topbar">
<div id="TopSection">
 <?php if ($site_name) { ?><h1 id='sitename'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?> 
<div id="topbarnav"> <span class="topnavitems"><?php print $header ?></span>
 <?php print $search_box ?>
</div></div>
<div class="clear"></div>
<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('id' => 'topmenu')) ?><?php } ?> 
</div>
</div>

<div id="wrap">
<div id="header">
<div id="logo"><?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?> </div>
<h2 class="introtext"> <?php if ($site_slogan) { ?><?php print $site_slogan ?><?php } ?> </h2>
</div>
<div id="contents">
<div class="clear"></div>
<div id="aboutdiv">
<?php if ($sidebar_left) { ?><?php print $sidebar_left ?><?php } ?> 
</div>
<div id="highlights">
<?php if ($sidebar_right) { ?><?php print $sidebar_right ?><?php } ?> 
</div>
<div id="homecontents"> 
<?php print $breadcrumb ?>
        <h2 class="title"><?php print $title ?></h2>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php if ($messages) { print $messages; } ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?> 

</div>



</div>
</div>
<div class="clear"></div>

<div id="footer">
<div id="footercontent">
<div id="previews">
<div class="item">
&nbsp; <!--nonbreaking space needed to avoid region disappearing if empty -->
<?php if ($bottom) { ?><?php print $bottom ?><?php } ?> 
 </div>
 <div class="clear"></div>
</div>
<div id="copyright">
<?php print $footer ?>
<br />
<?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?> 


</div>

</div>
<!--
Credits  should not be removied
Designed by Roshan, www.ramblingsoul.com
Drupal theme Ported by YurG, www.gomankov.ru/drupal
-->
<div id="credit">
    <?php print $footer_message ?> 
  <a title="Free Css Templates" href="http://www.ramblingsoul.com">CSS Template</a> by Rambling Soul | <a href="http://gomankov.ru/drupal"> Drupal Theme Port </a> by YurG.</div>
<!--Credits  should not be removied-->
<?php print $closure ?>
</body>
</html>