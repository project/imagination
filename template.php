<?php
function imagination_regions() {
  return array(
       'left' => t('left sidebar'),
       'right' => t('right sidebar'),
	   'content' => t('content'),
       'header' => t('header'),
       'footer' => t('footer'),
	    'bottom' => t('Bottom region')
		  );
} 