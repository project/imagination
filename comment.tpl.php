<div class="commentlist<?php if (isset($comment->status) && $comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
    <?php if ($picture) {
    print $picture;
  } ?>
<cite><?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?><?php print $title; ?> </cite>
<div class="commentmeta"><?php print $submitted; ?></div>
<span class="commenttxt">
<?php print $content; ?>
     <?php if ($signature): ?>
      <div class="clear-block">
       <div>—</div>
       <?php print $signature ?>
      </div>
     <?php endif; ?>
    </span>
<div class="submitted"><b><?php print $links; ?></b></div>
</div>